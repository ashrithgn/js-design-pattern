/*###############################################################
    Naming convention Followed                                  #
    Class name : upper Camel Case                               #
    Function : lower camel case,                                #
    Bumpy case : for protected function and variables.          #
    Author : Ashrith g.n                                        #
#################################################################*/


/*###############################################################
This is the implementation of the prototype pattern             #
#################################################################*/

/**
 * constructor method to create new class
 * @param {[int]}
 * @param {[int]}
 */
window.MyMath = function(lhs, rhs) {
    this.lhs = lhs;
    this.rhs = rhs;
}

/**
 * prototype function to add two numbers
 * @return {[int]}
 */

MyMath.prototype.addition = function() {
    if (typeof this.lhs == "number" && typeof this.rhs == "number") {
        return this.lhs + this.rhs;
    } else {
        throw "Invalid Operands";
        return false;
    }
}
/**
 * prototype function to sub two numbers
 * @return {[int]}
 */

MyMath.prototype.subtraction = function() {
    if (typeof this.lhs == "number" && typeof this.rhs == "number") {
        return this.lhs - this.rhs;
    } else {
        throw "Invalid Operands";
        return false;
    }
}

/**
 * prototype pattern to divide two numbers
 * @return {[int]}
 */

MyMath.prototype.division = function() {

    if (typeof this.lhs == "number" && typeof this.rhs == "number") {
        return this.lhs / this.rhs;
    } else {
        throw "Invalid Operands";
        return false;
    }
}

/**
 * prototype function  to mul two numbers
 * @return {[int]}
 */

MyMath.prototype.multiplication = function() {
    if (typeof this.lhs == "number" && typeof this.rhs == "number") {
        return this.lhs * this.rhs;
    } else {
        throw "Invalid Operands";
        return false;
    }
}

/**
 * prototype function to find modulus of  two numbers
 * @return {[int]}
 */

MyMath.prototype.modulus = function() {
    if (typeof this.lhs == "number" && typeof this.rhs == "number") {
        return this.lhs % this.rhs;
    } else {
        throw "Invalid Operands";
        return false;
    }
}

/**
 * prototype function to find power of number
 * @return {[int]}
 */
MyMath.prototype.power = function() {
    if (typeof this.lhs == "number" && typeof this.rhs == "number") {
        return Math.pow(lhs, rhs);
    } else {
        throw "Invalid Operands";
        return false;
    }
}

/**
 * setter method
 * @param {[int]}
 * @param {[int]}
 */
MyMath.prototype.set = function(lhs, rhs) {
    this.lhs = lhs;
    this.rhs = rhs;
}
