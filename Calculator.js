/*###############################################################
    Naming convention Followed                                  #
    Class name : upper Camel Case                               #
    Function : lower camel case,                                #
    Bumpy case : for protected function and variable.           #
    Author : Ashrith g.n                                        #
#################################################################*/



/*###############################################################
    Note: This demo application may contain logical bug,         #
    only few positive  scenario has been  implemented,           #
    to show the example of the modular design pattern.           #
#################################################################*/


window.Calculator = (function() {
    var _leftOperand = 0;
    var _rightOperand = 0;
    var _operation = '';
    var _logType = 'console';
    var _result = 0;
    var  _math = {}; 

    /**
     * this function is used to initialize the basic actions,
     * int this implementation we have initialized the writer
     * @param  {[String]}
     * @return {[void]}
     */

    function init(logType) {
        _logType = logType;
        _math = new MyMath(0, 0); // injecting dependencies
    }

    /**
     * take input and and assign the lhs rhs and operations and return result if calculated
     * @param  {[number || string]}
     * @return {[object]}
     */

    function calculate(input) {
        var operationArray = ["+", "-", "%", "mod", "/","*"];

        if (typeof input == "string" && _leftOperand != 0 && operationArray.indexOf(input) != -1) {
            _result = 0;
            _operation = input;
        } else if (_leftOperand == 0 && typeof input == "number") {
            _result = 0;
            _leftOperand = input;

        } else if (_rightOperand == 0 && typeof input == "number" && _operation != '') {
            _result = 0;
            _rightOperand = input
        } else if (_leftOperand != 0 && _operation == '') {
            _result = 0;
            _leftOperand = input;
        }

        if (input == "Result" || input == "result" || input == '=') {

            if (_leftOperand != 0 && _operation != '') {
                var result;
                switch (_operation) {
                    case '+':
                        result = _add(_leftOperand, _rightOperand);

                        if (result.status == true) {
                            _setDefaults();
                            calculate(result.value);
                        }
                        break;
                    case '-':
                        result = _sub(_leftOperand, _rightOperand);
                        if (result.status == true) {
                            _setDefaults();
                            calculate(result.value);
                        }
                        break;
                    case '/':
                        result = _div(_leftOperand, _rightOperand);
                        if (result.status == true) {
                            _setDefaults();
                            calculate(result.value);
                        }
                        break;
                    case 'mod':
                        result = _mod(_leftOperand, _rightOperand);
                        if (result.status == true) {
                            _setDefaults();
                            calculate(result.value);
                        }
                        break;
                    case '*':
                        result = _mul(_leftOperand, _rightOperand);
                        if (result.status == true) {
                            _setDefaults();
                            calculate(result.value);
                        }
                        break;
                    case 'pow':
                        result = _pow(_leftOperand, _rightOperand);
                        if (result.status == true) {
                            _setDefaults();
                            calculate(result.value);
                        }
                        break;
                    default:
                        exception('Soory invalid operation');
                }
            }

            return {
                'lhs': result.lhs,
                'rhs': result.rhs,
                'operation': result.operation,
                'result': result.value
            }
        } else {

            return {
                'lhs': _leftOperand,
                'rhs': _rightOperand,
                'operation': _operation,
                'result': _result
            }
        }


    }
    /**
     * private function to add two numbers
     * @param {[int][int]}
     * @return {[object]}
     */
    
    function _add(lop, rop) {
        _math.set(lop, rop);
        result = _math.addition();
        return {
            'status': true,
            'value': result,
            'lhs': lop,
            'rhs': rop,
            'operation': '+'
        }
    }


    /**
     * private function to Sub two numbers
     * @param {[int][int]}
     * @return {[object]}
     */
    
    function _sub(lop, rop) {
        _math.set(lop, rop);
        result = _math.subtraction();
        return {
            'status': true,
            'value': result,
            'lhs': lop,
            'rhs': rop,
            'operation': '-'
        }
    }


    /**
     * private function to mul two numbers
     * @param {[int][int]}
     * @return {[object]}
     */

    function _mul(lop, rop) {
        _math.set(lop, rop);
        result = _math.multiplication();
        return {
            'status': true,
            'value': result,
            'lhs': lop,
            'rhs': rop,
            'operation': '*'
        }
    }


    /**
     * private function to divide  two numbers
     * @param {[int][int]}
     * @return {[object]}
     */
    function _div(lop, rop) {
        _math.set(lop, rop);
        result = _math.division();

        return {
            'status': true,
            'value': result,
            'lhs': lop,
            'rhs': rop,
            'operation': '/'
        }
    }

    /**
     * private function to find mod of  two numbers
     * @param {[int][int]}
     * @return {[object]}
     */
    function _mod(lop, rop) {
        _math.set(lop, rop);
        result = _math.modulus();

        return {
            'status': true,
            'value': result,
            'lhs': lop,
            'rhs': rop,
            'operation': '%'
        }
    }


    /**
     * private function to lop pow rop
     * @param {[int][int]}
     * @param {[object]}
     */

    function pow(lop, rop) {
        _math.set(lop, rop);
        result = _math.power();
        return {
            'status': true,
            'value': result,
            'lhs': lop,
            'rhs': rop,
            'operation': '^'
        }
    }

    /**
     * private function to reset initialized value values
     * 
     */

    function _setDefaults() {
        _operation = '';
        _leftOperand = 0;
        _rightOperand = 0;
    }

    /**
     * throw and console log the msg based on initialization
     * @param  {[string]}
     * @return {[null]}
     */

    function exception(msg) {
        switch (_logType) {
            case 'alert':
                window.alert(msg);
                break;
            default:
                console.log(msg, 'EXCEPTION');
                break;
        }
        throw exception;
    }



    return {
        'init': init,
        'calculate': calculate
    }


})();
