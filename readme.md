# What is patterns ?

> A pattern is a reusable solution that can be applied to commonly occurring problems in software design - in our case - in writing JavaScript web applications. Another way of looking at patterns are as templates for how we solve problems - ones which can be used in quite a few different situations.

* Why we use Patterns ?
	* Patterns are proven solutions
	* Patterns can be easily reused
	* Patterns can be expressive

* Common Questions to be  Answered while implementing a Pattern ?
	*  how is the pattern considered successful?
	*  why is the pattern considered successful?
	*  what is Applicability of this pattern ?


# Few commonly used  js pattern :


## Module Pattern :

> The basic design pattern I’ll be using here is the Revealing Module Pattern, which is a variation of the classic Module Pattern, but with more control.
Which provide object oriented Approach , and provides the flexibility to make function public and private.
And helps to implement Dry(Do not repeat your self) design principle and  may be even Kiss principle so which makes our Code Base Solid Some extent like Single feature, open for extension closed for updated.

### Advantages :

* The module pattern is widely used because it provides structure and helps organize your code as it grows. Unlike other languages, JavaScript doesn’t have special syntax for packages, but the module pattern provides the tools to create self-contained decoupled pieces of code, which can be treated as black boxes of functionality and added, replaced, or removed according to the (ever-changing) requirements of the software you’re writing.
This pattern has combination of several pattern listed below
	
	* Namespaces
	* Immediate functions
	* Private and privileged members
	* Declaring dependencies

* It’s easier to see at a glance the methods that get exposed; when you’re not defining all your methods within return {} it means it’s one exposed function per line, making it easier to scan.

* You can expose methods via shorter names (eg add) but define them slightly more verbosely in your definition (eg returning the object instead of number).


 * Few Security advantages like private variables cannot be modified in console of browser.

### Disadvantage :

* Tedious to separate the ui and logical implantation
* Not so convenient to debug Errors
* Documentation is necessary to understand.

See the implemented [code](Calculator.js)

## Prototype Pattern :
> prototype pattern as one which creates objects based on a template of an existing object through cloning.
We can think of the prototype pattern as being based on prototype inheritance where we create objects which act as prototypes for other objects. The prototype object itself is effectively used as a blueprint for each object the constructor creates. If the prototype of the constructor function used contains a property called name for example then each object created by that same constructor will also have this same property.

### Advantages :

* when defining a function in an object, they're all created by reference.
* couple interactions as well as generalize
* this can be used as interface.
* helpful in data driven Application ie.Calculation 


### Disadvantage :

* A drawback to using the Prototype is that making a copy of an object can
sometimes be complicated.

See the implemented [code](MyMath.js)





 